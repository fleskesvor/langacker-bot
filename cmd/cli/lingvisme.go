package main

import (
	"fmt"
	"gitlab.com/fleskesvor/lingvisme/internal/factory"
)

func main() {
	pf := factory.NewParagraphFactory()
	fmt.Println(pf.Paragraph())
}
