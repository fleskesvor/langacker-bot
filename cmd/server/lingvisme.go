package main

import (
	"fmt"
	"gitlab.com/fleskesvor/lingvisme/internal/factory"
	"log"
	"net/http"
	"text/template"
)

var paragraphFactory *factory.ParagraphFactory
var htmlTemplate *template.Template

type Paragraph struct {
	Section string `json:"section"`
	Content string `json:"content"`
}

func main() {
	paragraphFactory = factory.NewParagraphFactory()
	htmlTemplate, _ = template.New("lingvisme").Parse(html())

	http.HandleFunc("/", lingvisme)
	fmt.Println("Server started on port 8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func lingvisme(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "text/html")
	_ = htmlTemplate.Execute(w, Paragraph{
		Section: paragraphFactory.Section(),
		Content: paragraphFactory.Paragraph().String(),
	})
}

func html() string {
	return `
<html>
<head>
    <title>Lingvisme</title>
    <style type="text/css">
        body {
            margin:10px;
            width: 600px;
            height:100%;
        }
        hr {
            color: gray;
        }
        div.normal {
            font-size: 18px;
        }
        div.small {
            font-size: 12px;
        }
    </style>
</head>
<body>


<h3>{{ .Section }}</h3>

<div class="normal">{{ .Content }}</div>

<hr/>
<div class="small">If you have any questions about this bot, feel free to contact
    one of its creators at elipanne (at) gmail dot com</div>
</body>
</html>
`
}