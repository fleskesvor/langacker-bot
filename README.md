# langacker-bot

A novelty script to create random paragraphs put together from authentic fragments of text written by the creator of
Cognitive Grammar, [Ronald Langacker](https://en.wikipedia.org/wiki/Ronald_Langacker).

## How to use

### CLI

`go run cmd/cli/lingvisme.go`

### HTTP Server

`go run cmd/server/lingvisme.go`

## TODO

* Remove risk of using the same fragments more than once.
* Create as reusable class, with optional min/max # of sentences.
* If used more than once, do we also want to avoid duplicates?
* If we remove fragments from db after use, handle large values of max.
