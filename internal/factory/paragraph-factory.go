package factory

import (
	"gitlab.com/fleskesvor/lingvisme"
	"gitlab.com/fleskesvor/lingvisme/internal/factory/static"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

const minSentences, maxSentences = 5, 10

type ParagraphFactory struct {
	IPF *static.InitPhraseFactory
	SPF *static.SubjPhraseFactory
	VPF *static.VerbPhraseFactory
	TPF *static.TermPhraseFactory
}

func NewParagraphFactory() *ParagraphFactory {
	rand.Seed(time.Now().UnixNano())
	return &ParagraphFactory{
		IPF: static.NewInitPhraseFactory(),
		SPF: static.NewSubjPhraseFactory(),
		VPF: static.NewVerbPhraseFactory(),
		TPF: static.NewTermPhraseFactory(),
	}
}

func (f *ParagraphFactory) Paragraph() lingvisme.Paragraph {
	var sentences []lingvisme.Sentence

	for num := 1; num <= intInRangeInclusive(minSentences, maxSentences); num++ {
		sentence, _ := f.Sentence(num == 1)
		sentences = append(sentences, sentence)
	}

	return lingvisme.Paragraph{Sentences: sentences}
}

func (f *ParagraphFactory) Sentence(isTopic bool) (lingvisme.Sentence, error) {
	isSingular := rand.Intn(2) == 1
	var initPhrase, subjPhrase, verbPhrase, termPhrase string
	var err error

	if initPhrase, err = f.IPF.InitPhrase(isTopic); err != nil {
		return lingvisme.Sentence{}, err
	}
	if subjPhrase, err = f.SPF.SubjPhrase(isSingular); err != nil {
		return lingvisme.Sentence{}, err
	}
	if verbPhrase, err = f.VPF.VerbPhrase(isSingular); err != nil {
		return lingvisme.Sentence{}, err
	}
	if termPhrase, err = f.TPF.TermPhrase(); err != nil {
		return lingvisme.Sentence{}, err
	}

	return lingvisme.Sentence{
		InitPhrase: initPhrase,
		SubjPhrase: subjPhrase,
		VerbPhrase: verbPhrase,
		TermPhrase: termPhrase,
	}, nil
}

func (f *ParagraphFactory) Section() string {
	return strings.Join([]string{
		strconv.Itoa(intInRangeInclusive(1, 9)),
		strconv.Itoa(intInRangeInclusive(1, 9)),
		strconv.Itoa(intInRangeInclusive(1, 9)),
	}, ".")
}

func intInRangeInclusive(min, max int) int {
	return rand.Intn(max-min+1) + min
}
