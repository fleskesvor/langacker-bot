package static

import (
	"errors"
	"math/rand"
)

type InitPhraseFactory struct {
	topicPhrases    []string
	nonTopicPhrases []string
}

func NewInitPhraseFactory() *InitPhraseFactory {
	return &InitPhraseFactory{
		topicPhrases: []string{
			"CG advances the controversial (if not outrageous) proposal that",
			"In performative use,",
			"First,",
			"A crucial point is that",
			"It is commonly assumed that",
			"In Cognitive Grammar,",
			"In practice,",
			"By assumption,",
			"In this formula,",
			"In any event,",
			"A crucial aspect of the usage-based approach is the notion that",
			"Of course,",
			"We have now seen numerous cases where",
			"From a symbolic perspective,",
			"By definition,",
			"I suggested in 5.3.2 that",
			"It is worth pausing to consider why",
			"As defined in Cognitive Grammar,",
			"I have suggested that",
			"I take it as self-evident that",
			"It is readily seen that",
			"In a usage-based model,",
			"Observe that",
		},
		nonTopicPhrases: []string{
			"Analogously,",
			"Similarly,",
			"The basic reality is simply that",
			"Typically, however,",
			"Canonically, then,",
			"The proposal, then, is that",
			"It must nevertheless be admitted that",
			"Thus,",
			"In addition,",
			"I demonstrate the inexorable progress of grammatical theory by claiming that",
			"We now have an explanation for why",
			"However,",
			"I will assume, then, that",
			"We can now begin to examine how",
			"In short,",
			"To state it directly, I claim that",
			"Moreover,",
			"We must now consider whether",
			"We see, then, that",
			"Also, it is not implausible to suppose that",
		},
	}
}

func (f *InitPhraseFactory) InitPhrase(isTopic bool) (string, error) {
	if len(f.topicPhrases) < 1 || len(f.nonTopicPhrases) < 1 {
		return "", errors.New("no init phrases found")
	} else if isTopic {
		index := rand.Intn(len(f.topicPhrases))
		return f.topicPhrases[index], nil
	} else {
		index := rand.Intn(len(f.nonTopicPhrases))
		return f.nonTopicPhrases[index], nil
	}
}
