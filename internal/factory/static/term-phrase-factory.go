package static

import (
	"errors"
	"math/rand"
)

type TermPhraseFactory struct {
	phrases []string
}

func NewTermPhraseFactory() *TermPhraseFactory {
	return &TermPhraseFactory{
		phrases: []string{
			"cognitive processing in general.",
			"actual language use.",
			"processual relationships.",
			"full compositionality.",
			"unipolar elements.",
			"indefinitely many instances of the [TABLE] category.",
			"the conventional units of a language.",
			"conventional semantic values.",
			"linguistic meaning.",
			"the traditional parts of speech.",
			"communicative intent.",
			"the situation described.",
			"the immediate temporal scope.",
			"symbolic complexity.",
			"the composite-structure level.",
			"what is traditionally called a head.",
			"the composite structure.",
			"the symbolic assemblies of CG.",
			"recurring aspects of processing activity.",
		},
	}
}

func (f *TermPhraseFactory) TermPhrase() (string, error) {
	if len(f.phrases) < 1 {
		return "", errors.New("no term phrases found")
	} else {
		index := rand.Intn(len(f.phrases))
		return f.phrases[index], nil
	}
}
