package static

import (
	"errors"
	"math/rand"
)

type VerbPhraseFactory struct {
	singularPhrases         []string
	pluralPhrases           []string
	singularOrPluralPhrases []string
}

func NewVerbPhraseFactory() *VerbPhraseFactory {
	return &VerbPhraseFactory{
		singularPhrases: []string{
			"is intrinsically associated with",
			"is then describable as the context necessary for the characterization of",
			"is provided by",
			"defines the potential for",
			"profiles",
			"corresponds to",
			"has special status as",
			"is formed by",
			"is often not a regular function of",
			"is identical to the single conceptualization conveyed by",
			"furnishes the primary content of",
			"is crucial for",
			"is autonomous and makes no essential reference to",
			"correlates with",
			"is also affected by",
			"correlates with",
			"approximates",
			"is roughly equivalent to",
			"participates in a categorizing relationship with",
		},
		pluralPhrases: []string{
			"are commonly recognized as",
			"are both the cradle of language and the crucible of",
			"designate different kinds of",
			"reduce in essence to",
			"are distinct and salient entities differentiated from their surroundings and from",
			"are basically extrinsic specifications and are neither prominent nor central to",
			"do not themselves incorporate",
			"occur in a certain temporal sequence, which linguists refer to as",
			"correspond to",
			"are a consequence of",
			"are independent of",
			"can be used in constructing and understanding",
		},
		singularOrPluralPhrases: []string{
			"cannot provide a fixed, unitary expression for",
			"will tend to gravitate toward",
		},
	}
}

func (f *VerbPhraseFactory) VerbPhrase(isSingular bool) (string, error) {
	if len(f.singularPhrases) < 1 || len(f.pluralPhrases) < 1 {
		return "", errors.New("no verb phrases found")
	} else if isSingular {
		combined := append(f.singularPhrases, f.singularOrPluralPhrases...)
		index := rand.Intn(len(combined))
		return combined[index], nil
	} else {
		combined := append(f.pluralPhrases, f.singularOrPluralPhrases...)
		index := rand.Intn(len(combined))
		return combined[index], nil
	}
}
