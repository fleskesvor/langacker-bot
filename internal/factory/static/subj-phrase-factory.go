package static

import (
	"errors"
	"math/rand"
)

type SubjPhraseFactory struct {
	singularPhrases []string
	pluralPhrases   []string
}

func NewSubjPhraseFactory() *SubjPhraseFactory {
	return &SubjPhraseFactory{
		singularPhrases: []string{
			"the polysemy of lexical items",
			"a usage-based model",
			"a higher-level schema",
			"the highest-level constructional schema",
			"the target of categorization",
			"a head",
			"the definite article",
			"a language",
			"a coherent composite structure",
			"the inventory of fixed expressions in a language",
			"constructional meaning",
			"the high-level ditransitive schema [V NML NML]",
			"a lexical item's occurence in larger structural contexts",
			"the asymmetry between autonomous and dependent components",
			"a component structure",
			"a construction's profile determinant",
			"a relational predicate whose profile details the interconnections between two distinct entities",
			"a semantic structure",
			"the subject",
			"the scope of a predication",
			"the stative character of a relation",
			"a fixed expression",
			"a lexical unit",
			"the substructure profiled by a relational predicate",
			"the vantage point assumed for linguistic purposes",
			"objective construal",
			"a conceptual account of meaning",
			"the grammatical distinction between count and mass nouns",
			"grammar",
		},
		pluralPhrases: []string{
			"phonological units",
			"the rules and restrictions of a language",
			"basic grammatical categories",
			"certain mass nouns",
			"the reference point and target",
			"conventional expressions",
			"the words of an expression",
			"basic conceptual relations",
			"the specifications of the sanctioning and target structures",
			"lexicon and grammar",
			"the lexicon of a language",
			"complex expressions",
			"lower-level schemas, i.e. structures with greater specificity,",
			"cognitive \"routines \"",
			"the elements usually regarded as grammatical morphemes",
			"these two elaborative relationships",
			"phonogical elements of roughly comparable size",
			"these schemas",
			"usage events",
			"the semantic structures under discussion",
			"the conventions of a language",
		},
	}
}

func (f *SubjPhraseFactory) SubjPhrase(isSingular bool) (string, error) {
	if len(f.singularPhrases) < 1 || len(f.pluralPhrases) < 1 {
		return "", errors.New("no subj phrases found")
	} else if isSingular {
		index := rand.Intn(len(f.singularPhrases))
		return f.singularPhrases[index], nil
	} else {
		index := rand.Intn(len(f.pluralPhrases))
		return f.pluralPhrases[index], nil
	}
}
