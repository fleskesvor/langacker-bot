package lingvisme

import (
	"fmt"
	"strings"
)

type Paragraph struct {
	Sentences []Sentence
}

type Sentence struct {
	InitPhrase string
	SubjPhrase string
	VerbPhrase string
	TermPhrase string
}

type InitPhraseFactory interface {
	InitPhrase(isTopic bool) (string, error)
}

type SubjPhraseFactory interface {
	SubjPhrase(isSingular bool) (string, error)
}

type VerbPhraseFactory interface {
	VerbPhrase(isSingular bool) (string, error)
}

type TermPhraseFactory interface {
	TermPhrase() (string, error)
}

type ParagraphFactory interface {
	Paragraph() Paragraph
	Sentence(isTopic bool) (Sentence, error)
	Section() string
}

func (p Paragraph) String() string {
	return p.Join(" ")
}

func (p Paragraph) Join(sep string) string {
	sentences := make([]string, len(p.Sentences))
	for index, sentence := range p.Sentences {
		sentences[index] = sentence.String()
	}
	return strings.Join(sentences, sep)
}

func (s Sentence) String() string {
	return fmt.Sprintf("%s %s %s %s", s.InitPhrase, s.SubjPhrase, s.VerbPhrase, s.TermPhrase)
}
